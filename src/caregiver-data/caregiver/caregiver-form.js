import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import TextInput from "../../user-data/user/fields/TextInput";


class CaregiverForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {

                caregiver_id: {
                    value: '',
                    placeholder: 'caregiver_id',
                    valid: false,
                    touched: false,
                },

                address: {
                    value: '',
                    placeholder: 'address',
                    valid: false,
                    touched: false,

                },
                birthdate: {
                    value: '',
                    placeholder: 'birthdate',
                    valid: false,
                    touched: false,

                },
                gender: {
                    value: '',
                    placeholder: 'gender',
                    valid: false,
                    touched: false,
                },

                name:{
                    value: '',
                    placeholder: 'name',
                    valid:false,
                    touched:false,
                },

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerCaregiver(caregiver){
        return API_USERS.addCaregiver(caregiver, (result, status, error) => {
            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted caregiver with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateCaregiver(caregiver){
        return API_USERS.updateCaregiver(caregiver, (result, status, error) => {
            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted caregiver with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    deleteCaregiver(id){
        return API_USERS.deleteCaregiver(id, (result, status, error) => {
            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted caregiver with id: " + result);
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleUpdate(){
        let caregiver = {
            caregiver_id: this.state.formControls.caregiver_id.value,
            address: this.state.formControls.address.value,
            birthdate: this.state.formControls.birthdate.value,
            gender: this.state.formControls.gender.value,
            name: this.state.formControls.name.value,
        };

        this.updateCaregiver(caregiver);
    }

    handleSubmit(){

        let caregiver = {
            caregiver_id: this.state.formControls.caregiver_id.value,
            address: this.state.formControls.address.value,
            birthdate: this.state.formControls.birthdate.value,
            gender: this.state.formControls.gender.value,
            name: this.state.formControls.name.value,
        };

        this.registerCaregiver(caregiver);
    }

    handleDelete(){
        let id = {caregiver_id: this.state.formControls.caregiver_id.value};
        this.deleteCaregiver(id);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>

                <h1>Add new caregiver</h1>

                <p> caregiver_id: </p>
                <TextInput name="caregiver_id"
                           placeholder={this.state.formControls.caregiver_id.placeholder}
                           value={this.state.formControls.caregiver_id.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.caregiver_id.touched}
                           valid={this.state.formControls.caregiver_id.valid}
                />

                <p> address: </p>
                <TextInput name="address"
                           placeholder={this.state.formControls.address.placeholder}
                           value={this.state.formControls.address.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.address.touched}
                           valid={this.state.formControls.address.valid}
                />

                <p> birthdate: </p>
                <TextInput name="birthdate"
                           placeholder={this.state.formControls.birthdate.placeholder}
                           value={this.state.formControls.birthdate.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.birthdate.touched}
                           valid={this.state.formControls.birthdate.valid}
                />

                <p> gender: </p>
                <TextInput name="gender"
                           placeholder={this.state.formControls.gender.placeholder}
                           value={this.state.formControls.gender.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.gender.touched}
                           valid={this.state.formControls.gender.valid}
                />

                <p> name: </p>
                <TextInput name="name"
                           placeholder={this.state.formControls.name.placeholder}
                           value={this.state.formControls.name.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.name.touched}
                           valid={this.state.formControls.name.valid}
                />



                <p></p>
                <Button variant="success"
                        type={"submit"}
                >
                    Add
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

                <p></p>
                <div>
                    <Button
                        variant="success"
                        type={"update"}
                    >
                        Update
                    </Button>
                </div>

                <p></p>
                <div>
                    <Button
                        variant="success"
                        type={"delete"}
                    >
                        Delete
                    </Button>
                </div>

            </form>


        );
    }
}

export default CaregiverForm;
