import React from 'react';
import {Card, Col, Row} from 'reactstrap';


import * as API_PATIENTS from "./caregiver-data/caregiver/api/caregiver-role-api"
import Table from "./commons/tables/table";
import APIResponseErrorMessage from "./commons/errorhandling/api-response-error-message";

import Button from "react-bootstrap/Button";
import TextInput from "./user-data/user/fields/TextInput";

const columns = [
    {
        Header:  'patient_id',
        accessor: 'patient_id',
    },
    {
        Header: 'address',
        accessor: 'address',
    },
    {
        Header: 'birthdate',
        accessor: 'birthdate',
    },
    {
        Header: 'gender',
        accessor: 'gender',
    },
    {
        Header: 'medicalrecord',
        accessor: 'medicalrecord',
    },
    {
        Header: 'name',
        accessor: 'name',
    },
];

const filters = [];

class CaregiverRole extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            caregiver_id: {
                value: '',
                placeholder: 'id',
                valid: false,
                touched: false,
            },
            error: null
        };
        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchPatients();
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        return API_PATIENTS.getPatientsByCaregiver_id(event.target.value,(result, status, err) => {
            console.log(result);
            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        patient_id: x.patient_id,
                        address: x.address,
                        birthdate: x.birthdate,
                        gender: x.gender,
                        medicalrecord: x.medicalrecord,
                        name: x.name,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };


    fetchPatients(caregiver_id) {

    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>


                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <h1>Find patients</h1>

                <p> caregiver_id: </p>
                <TextInput name="caregiver_id"
                           placeholder={this.state.caregiver_id.placeholder}
                           value={this.state.caregiver_id.value}
                           onChange={this.handleChange}
                           touched={this.state.caregiver_id.touched}
                           valid={this.state.caregiver_id.valid}
                />

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };
}

export default CaregiverRole;
