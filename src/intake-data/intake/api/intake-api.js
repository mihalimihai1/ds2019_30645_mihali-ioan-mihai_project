import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_intake: '/intake/',
    post_intake: "/intake/"
};

function getIntake(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_intake, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getIntakeById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_intake + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function addIntake(intake, callback){
    let request = new Request(HOST.backend_api + endpoint.post_intake , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(intake)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getIntake,
    getIntakeById,
    addIntake
};
