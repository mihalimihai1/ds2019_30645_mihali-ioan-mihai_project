import React from 'react';
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/intake-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import TextInput from "../../user-data/user/fields/TextInput";



class IntakeForm extends React.Component{

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls : {

                intake_id: {
                    value: '',
                    placeholder: 'id',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 1,
                        isRequired: true
                    }
                },

                end: {
                    value: '',
                    placeholder: 'Email...',
                    valid: false,
                    touched: false,
                },

                start: {
                    value: '',
                    placeholder: 'start',
                    valid: false,
                    touched: false,

                },
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {

    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerIntake(intake){
        return API_USERS.addIntake(intake, (result, status, error) => {
            console.log(result);

            if(result !== null && (status === 200 || status ===201)){
                console.log("Successfully inserted intake with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }



    handleSubmit(){
        let intake = {
            intake_id: this.state.formControls.intake_id.value,
            end : this.state.formControls.end.value,
            start: this.state.formControls.start.value,
        };

        this.registerIntake(intake);
    }

    render() {
        return (

            <form onSubmit={this.handleSubmit}>

                <h1>Insert new intake</h1>

                <p> intake_id: </p>

                <TextInput name="intake_id"
                           placeholder={this.state.formControls.intake_id.placeholder}
                           value={this.state.formControls.intake_id.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.intake_id.touched}
                           valid={this.state.formControls.intake_id.valid}
                />

                <p> end: </p>
                <TextInput name="end"
                           placeholder={this.state.formControls.end.placeholder}
                           value={this.state.formControls.end.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.end.touched}
                           valid={this.state.formControls.end.valid}
                />

                <p> start: </p>
                <TextInput name="start"
                           placeholder={this.state.formControls.start.placeholder}
                           value={this.state.formControls.start.value}
                           onChange={this.handleChange}
                           touched={this.state.formControls.start.touched}
                           valid={this.state.formControls.start.valid}
                />

                <p></p>
                <Button variant="success"
                        type={"submit"}
                        >
                    Submit
                </Button>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </form>

        );
    }
}

export default IntakeForm;
