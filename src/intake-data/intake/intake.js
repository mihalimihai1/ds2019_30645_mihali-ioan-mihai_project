import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import * as API_USERS from "./api/intake-api"
import IntakeForm from "./intake-form";

const columns = [
    {
        Header:  'intake_id',
        accessor: 'intake_id',
    },
    {
        Header: 'start',
        accessor: 'start',
    },
    {
        Header: 'end',
        accessor: 'end',
    },
];

const filters = [];

class Intake extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchIntake();
    }

    fetchIntake() {
        return API_USERS.getIntake((result, status, err) => {

            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        intake_id: x.intake_id,
                        end: x.end,
                        start: x.start,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <IntakeForm
                                    registerIntake={this.refresh}>
                                </IntakeForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Intake;
