package com.example.springdemo.controller;

import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value="/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }
    @GetMapping(value="/{username}/{password}")
    public ResponseEntity<?> Login(@PathVariable String username, @PathVariable String password){
        try {
            String x= userService.Login(username, password);
            return new ResponseEntity<>(x, HttpStatus.OK);
        }
        catch (Exception e){
            return new ResponseEntity<>(e,HttpStatus.BAD_REQUEST);
        }
    }
}
