package com.example.springdemo.dto;

import com.example.springdemo.entities.Doctor;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;

import java.security.Timestamp;
import java.util.List;

public class CaregiverDTO {
    private Integer caregiver_id;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private DoctorDTO doctor;
    private List<PatientDTO> patientList;

    public CaregiverDTO(Integer caregiver_id, DoctorDTO doctor, String name, String birthdate, String gender, String address, List<PatientDTO> patientList) {
        this.caregiver_id = caregiver_id;
        this.doctor = doctor;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patientList = patientList;
    }

    public CaregiverDTO(Integer caregiver_id, String name, String birthdate, String gender, String address, List<PatientDTO> patientList) {
        this.caregiver_id = caregiver_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patientList = patientList;
    }

    public CaregiverDTO() {
    }

    public CaregiverDTO(Integer caregiver_id, String name, String birthdate, String gender, String address) {
        this.caregiver_id = caregiver_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getCaregiver_id() {
        return caregiver_id;
    }

    public void setCaregiver_id(Integer caregiver_id) {
        this.caregiver_id = caregiver_id;
    }

    public DoctorDTO getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDTO doctor) {
        this.doctor = doctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PatientDTO> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<PatientDTO> patientList) {
        this.patientList = patientList;
    }
}
