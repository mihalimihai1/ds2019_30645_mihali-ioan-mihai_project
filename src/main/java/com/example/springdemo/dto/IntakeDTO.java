package com.example.springdemo.dto;

public class IntakeDTO {
    private Integer intake_id;
    private String start;
    private String end;

    public IntakeDTO(Integer intake_id, String start, String end) {
        this.intake_id = intake_id;
        this.start = start;
        this.end = end;
    }

    public IntakeDTO() {
    }

    public Integer getIntake_id() {
        return intake_id;
    }

    public void setIntake_id(Integer intake_id) {
        this.intake_id = intake_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
