package com.example.springdemo.dto;

public class MedicationsDTO {
    private Integer medications_id;
    private String name;
    private String list_effects;
    private Integer dosage;

    public MedicationsDTO(Integer medications_id, String name, String list_effects, Integer dosage) {
        this.medications_id = medications_id;
        this.name = name;
        this.list_effects = list_effects;
        this.dosage = dosage;
    }

    public MedicationsDTO() {
    }

    public Integer getMedications_id() {
        return medications_id;
    }

    public void setMedications_id(Integer medications_id) {
        this.medications_id = medications_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getList_effects() {
        return list_effects;
    }

    public void setList_effects(String list_effects) {
        this.list_effects = list_effects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
