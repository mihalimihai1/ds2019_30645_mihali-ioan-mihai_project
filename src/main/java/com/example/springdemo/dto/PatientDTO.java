package com.example.springdemo.dto;

import java.security.Timestamp;

public class PatientDTO {
    private Integer patient_id;
    private String name;
    private String birthdate;
    private String gender;
    private String address;
    private String medicalrecord;
    private CaregiverDTO caregiver;

    public PatientDTO(Integer patient_id, String name, String birthdate, String gender, String address, String medicalrecord, CaregiverDTO caregiver) {
        this.patient_id = patient_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
        this.caregiver = caregiver;
    }

    public PatientDTO(Integer patient_id, String name, String birthdate, String gender, String address, String medicalrecord) {
        this.patient_id = patient_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
    }

    public PatientDTO() {
    }

    public  Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalrecord() {
        return medicalrecord;
    }

    public void setMedicalrecord(String medicalrecord) {
        this.medicalrecord = medicalrecord;
    }

    public CaregiverDTO getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(CaregiverDTO caregiver) {
        this.caregiver = caregiver;
    }
}
