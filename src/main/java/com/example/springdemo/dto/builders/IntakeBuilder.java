package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.entities.Intake;

public class IntakeBuilder {

    private IntakeBuilder(){
    }
    public static IntakeDTO generateDTOFromEntity(Intake intake) {
        IntakeDTO caregiverDTO = new IntakeDTO(
                intake.getIntake_id(),
                intake.getStart(),
                intake.getEnd()
        );
        return  caregiverDTO;
    }

    public static Intake generateEntityFromDTO(IntakeDTO intakeDTO) {
        return new Intake(
                intakeDTO.getIntake_id(),
                intakeDTO.getStart(),
                intakeDTO.getEnd()
        );
    }
}
