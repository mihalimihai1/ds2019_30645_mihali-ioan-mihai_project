package com.example.springdemo.entities;

import javax.persistence.*;
import javax.print.Doc;
import java.sql.Timestamp;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
    @Table(name = "caregiver")
    public class Caregiver {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "caregiver_id", unique = true, nullable = false)
    private Integer caregiver_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId", referencedColumnName = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "doctorId")
    private Doctor doctor;

    @Column(name = "name")
    private String name;

    @Column(name="birthdate")
    private String birthdate;

    @Column(name="gender")
    private String gender;

    @Column(name="address")
    private String address;

    @OneToMany(mappedBy = "caregiver")
    private List<Patient> patientList;

    public Caregiver() {
    }

    public Caregiver(Integer caregiver_id, User user, Doctor doctor, String name, String birthdate, String gender, String address, List<Patient> patientList) {
        this.caregiver_id = caregiver_id;
        this.user = user;
        this.doctor = doctor;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.patientList = patientList;
    }

    public Caregiver(Integer caregiver_id, String name, String birthdate, String gender, String address) {
        this.caregiver_id = caregiver_id;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }

    public Integer getCaregiver_id() {
        return caregiver_id;
    }

    public void setCaregiver_id(Integer caregiver_id) {
        this.caregiver_id = caregiver_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Patient> getPatientList() {
        return patientList;
    }

    public void setPatientList(List<Patient> patientList) {
        this.patientList = patientList;
    }
}
