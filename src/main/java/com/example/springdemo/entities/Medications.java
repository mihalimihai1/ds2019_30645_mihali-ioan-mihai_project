package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medications")
public class Medications {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medications_id", unique = true, nullable = false)
    private Integer medications_id;

    @Column(name = "name")
    private String name;

    @Column(name="list_effects")
    private String list_effects;

    @Column(name="dosage")
    private Integer dosage;

    @OneToOne(mappedBy = "medications")
    private Intake intake;

    public Medications() {
    }

    public Medications(Integer medications_id, String name, String list_effects, Integer dosage, Intake intake) {
        this.medications_id = medications_id;
        this.name = name;
        this.list_effects = list_effects;
        this.dosage = dosage;
        this.intake = intake;
    }

    public Medications(Integer medications_id,String name, String list_effects, Integer dosage) {
        this.medications_id = medications_id;
        this.name = name;
        this.list_effects = list_effects;
        this.dosage = dosage;
    }

    public Intake getIntake() {
        return intake;
    }

    public Integer getMedications_id() {
        return medications_id;
    }

    public void setMedications_id(Integer medications_id) {
        this.medications_id = medications_id;
    }

    public void setIntake(Intake intake) {
        this.intake = intake;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getList_effects() {
        return list_effects;
    }

    public void setList_effects(String list_effects) {
        this.list_effects = list_effects;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
