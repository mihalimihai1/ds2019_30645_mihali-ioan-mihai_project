package com.example.springdemo.rabbitmq;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class Consumer implements MessageListener {

    @Autowired
    private DataRepository dataRepository;

    @Override
    @RabbitListener(queues = "${rabbitmq.queue}")
    public void onMessage(Message message) {
        String myMessage = new String(message.getBody());
        JsonObject jsonObject = new Gson().fromJson(myMessage, JsonObject.class);

        Data data = new Data();
        data.setActivity((jsonObject.get("activity")).getAsString());
        data.setStart_time(jsonObject.get("start time").getAsString());
        data.setEnd_time(jsonObject.get("end time").getAsString());
        data.setId_patient(Integer.valueOf(jsonObject.get("id patient").getAsString()));
//        dataRepository.save(data);

        if (data.getActivity().contains("Sleeping")) {
            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime date1 = LocalDateTime.from(f.parse(data.getStart_time()));
            LocalDateTime date2 = LocalDateTime.from(f.parse(data.getEnd_time()));
            Duration d = Duration.between(date1, date2);
            if ((d.getSeconds() / 3600) > 9) {
                System.out.println("Pacientul a dormit mai mult de 9 ore");
            }
        }

        if (data.getActivity().contains("Leaving\t")) {
            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime date1 = LocalDateTime.from(f.parse(data.getStart_time()));
            LocalDateTime date2 = LocalDateTime.from(f.parse(data.getEnd_time()));
            Duration d = Duration.between(date1, date2);
            if ((d.getSeconds() / 3600) > 3) {
                System.out.println("Pacientul a fost plecat de acasa mai mult de 3 ore");
            }
        }

        if (data.getActivity().contains("Toileting\t") || data.getActivity().contains("Showering\t")) {
            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime date1 = LocalDateTime.from(f.parse(data.getStart_time()));
            LocalDateTime date2 = LocalDateTime.from(f.parse(data.getEnd_time()));
            Duration d = Duration.between(date1, date2);
            if ((d.getSeconds() / 60) >= 30) {
                System.out.println("Pacientul a stat in baie mai mult de 30 de minute");
            }
        }

//        System.out.println("Received message = " + data.toString());
    }
}
