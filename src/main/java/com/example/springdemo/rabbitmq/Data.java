package com.example.springdemo.rabbitmq;

import org.json.simple.JSONObject;

import javax.persistence.*;

@Entity
@Table(name = "data")
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "startTime")
    private String start_time;

    @Column(name = "endTime")
    private String end_time;

    @Column(name = "activity")
    private String activity;

    @Column(name = "idPatient")
    private Integer id_patient;

    public Data() {
    }

    public Data(String start_time, String end_time, String activity, Integer id_patient) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
        this.id_patient = id_patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Integer getId_patient() {
        return id_patient;
    }

    public void setId_patient(Integer id_patient) {
        this.id_patient = id_patient;
    }

    public JSONObject toJson(Data data) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("activity", data.activity);
        jsonObject.put("start time", data.start_time);
        jsonObject.put("end time", data.end_time);
        jsonObject.put("id patient", data.id_patient);
        return jsonObject;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", start_time='" + start_time + '\'' +
                ", end_time='" + end_time + '\'' +
                ", id_patient=" + id_patient +
                '}';
    }
}
