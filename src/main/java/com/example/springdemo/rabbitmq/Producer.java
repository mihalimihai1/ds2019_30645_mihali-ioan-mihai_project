package com.example.springdemo.rabbitmq;

import org.json.simple.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Producer {
    @Value("${rabbitmq.queue}")
    private String queueName;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    @Autowired
    private AmqpTemplate amqpTemplate;

    ReadFromFile readFromFile = new ReadFromFile();

    public void produceMessage() throws InterruptedException {
        readFromFile.read();
        for (JSONObject mydata : readFromFile.myData) {
            amqpTemplate.convertAndSend(exchange, routingkey, mydata);
            Thread.sleep(500);
//            System.out.println("Send message = " + mydata);
        }
    }
}
