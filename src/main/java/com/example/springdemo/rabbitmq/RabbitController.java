package com.example.springdemo.rabbitmq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitController {
    @Autowired
    Producer producer;

    @GetMapping("/messages")
    public void send() throws InterruptedException {
        producer.produceMessage();
        System.out.println("Message sent");
    }
}
