package com.example.springdemo.rabbitmq;

import org.json.simple.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFromFile {

    private static final String file = "activity.txt.txt";
    ArrayList<JSONObject> myData;

    public ReadFromFile(){
        this.myData = new ArrayList<>();
    }

    public void read(){
        ArrayList<Data> data = new ArrayList<>();
        List<String> buf = null;

        try (Stream<String> stream = Files.lines(Paths.get(file))){
            buf = stream.collect(Collectors.toList());
        } catch (IOException e){
            e.printStackTrace();
        }

        for (String i : buf) {
            String[] split1 = i.split(" ");
            String[] split2 = split1[1].split("		");
            String[] split3 = split1[2].split("		");
            data.add(new Data(split1[0] + " " + split2[0], split2[1] + " " + split3[0], split3[1] + " ", 1));
        }

        for (Data data1: data){
            myData.add(data1.toJson(data1));
        }
    }

    public List<String> getActivities() {
        List<String> buf = null;
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            buf = stream.collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return buf;

    }
}

