package com.example.springdemo.repositories;

import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {
    public List<Patient> findAllByCaregiver(Caregiver caregiver);
    public Patient findByUser(User user);
    public Patient findByIntake(Intake intake);


    @Query(value = "SELECT p " +
            "FROM Patient p " +
            "ORDER BY p.name ")
    List<Patient> getAllOrdered();
}
