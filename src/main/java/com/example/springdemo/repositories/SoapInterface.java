package com.example.springdemo.repositories;


import com.example.springdemo.services.ClassWrapper;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface SoapInterface {

    @WebMethod
    public ClassWrapper getString();
}


