package com.example.springdemo.repositories;


import com.example.springdemo.entities.Patient;
import com.example.springdemo.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    public User findByUsername(String username);

    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.username = ?1 and u.password = ?2")
    User Login(String username, String password);
}
