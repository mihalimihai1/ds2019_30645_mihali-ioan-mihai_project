package com.example.springdemo.services;

import com.example.springdemo.rabbitmq.Producer;
import com.example.springdemo.rabbitmq.ReadFromFile;
import com.example.springdemo.repositories.SoapInterface;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "com.example.springdemo.repositories.SoapInterface")
public class ActivityService implements SoapInterface {

    Producer producer;
    public ClassWrapper getString() {
        ClassWrapper wrapper;

        ReadFromFile f =new ReadFromFile();
        List<String> activities = f.getActivities();
        wrapper=new ClassWrapper();
        wrapper.setMyList(activities);

        return wrapper;
    }


}
