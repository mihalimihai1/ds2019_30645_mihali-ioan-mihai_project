package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.dto.builders.PatientBuilder;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.entities.Patient;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.CaregiverRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository){
        this.caregiverRepository=caregiverRepository;
    }

    public CaregiverDTO findCaregiverById(Integer id){

        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<PatientDTO> findPatientsByCaregiver_id(Integer id){

        List<Patient> patients  = caregiverRepository.findPatientsByCaregiver_id(id);

        return patients.stream()
                .map(PatientBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public List<CaregiverDTO> findAll(){
        List<Caregiver> caregivers = caregiverRepository.getAllOrdered();

        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(CaregiverDTO caregiverDTO) {

        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getCaregiver_id();
    }

    public Integer update(CaregiverDTO caregiverDTO) {

        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getCaregiver_id());

        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverDTO.getCaregiver_id().toString());
        }

        return caregiverRepository.save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO)).getCaregiver_id();
    }

    public void delete(CaregiverDTO caregiverDTO){
        this.caregiverRepository.deleteById(caregiverDTO.getCaregiver_id());
    }

}
