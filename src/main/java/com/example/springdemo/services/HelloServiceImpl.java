package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;
import io.grpc.stub.StreamObserver;
import org.baeldung.grpc.DisplayRequest;
import org.baeldung.grpc.HelloServiceGrpc;
import org.baeldung.grpc.IntakePlan;
import org.baeldung.grpc.IntakeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelloServiceImpl extends HelloServiceGrpc.HelloServiceImplBase {

    @Autowired
    private  final IntakeService intakeService;

    public HelloServiceImpl(IntakeService intakeService) {
        this.intakeService = intakeService;
    }

    @Override
    public void display(IntakeRequest displayRequest, StreamObserver<IntakePlan> intakePlanStreamObserver){

     List<IntakeDTO> medicationPlan = intakeService.findIntake(displayRequest.getIdPatient());
     IntakePlan intakeResponse = IntakePlan.newBuilder().build();

     for(int i = 0; i < medicationPlan.size(); i++){

         IntakeDTO intakeDTO = medicationPlan.get(i);

         DisplayRequest displayRequest1 = DisplayRequest.newBuilder()
                 .setIntakeId(intakeDTO.getIntake_id())
                 .setStart(intakeDTO.getStart())
                 .setEnd(intakeDTO.getEnd())
                 .build();
         intakeResponse = IntakePlan.newBuilder(intakeResponse).addIntake(displayRequest1).build();
     }
     System.out.println(intakeResponse);

     intakePlanStreamObserver.onNext(intakeResponse);
     intakePlanStreamObserver.onCompleted();
    }
}
