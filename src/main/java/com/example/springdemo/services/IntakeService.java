package com.example.springdemo.services;

import com.example.springdemo.dto.IntakeDTO;
import com.example.springdemo.dto.builders.IntakeBuilder;
import com.example.springdemo.entities.Intake;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.IntakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IntakeService {

    private final IntakeRepository intakeRepository;

    @Autowired
    public IntakeService(IntakeRepository intakeRepository){
        this.intakeRepository = intakeRepository;
    }

    public List<IntakeDTO> findIntake(Integer idPatient) {
        List<Intake> medicationPlans = intakeRepository.findByPatient(idPatient);

        return medicationPlans.stream()
                .map(IntakeBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public IntakeDTO findIntakeById(Integer id){

        Optional<Intake> intake  = intakeRepository.findById(id);

        if (!intake.isPresent()) {
            throw new ResourceNotFoundException("Intake", "intake id", id);
        }
        return IntakeBuilder.generateDTOFromEntity(intake.get());
    }

    public List<IntakeDTO> findAll(){
        List<Intake> intakes = intakeRepository.getAllOrdered();

        return intakes.stream()
                .map(IntakeBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer insert(IntakeDTO intakeDTO) {

        return intakeRepository
                .save(IntakeBuilder.generateEntityFromDTO(intakeDTO))
                .getIntake_id();

    }

    public Integer update(IntakeDTO intakeDTO) {

        Optional<Intake> intake = intakeRepository.findById(intakeDTO.getIntake_id());

        if(!intake.isPresent()){
            throw new ResourceNotFoundException("Intake", "intake id", intakeDTO.getIntake_id().toString());
        }

        return intakeRepository.save(IntakeBuilder.generateEntityFromDTO(intakeDTO)).getIntake_id();
    }

    public void delete(IntakeDTO intakeDTO){
        this.intakeRepository.deleteById(intakeDTO.getIntake_id());
    }
}
