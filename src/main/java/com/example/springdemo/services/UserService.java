package com.example.springdemo.services;

import com.example.springdemo.entities.User;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String Login(String username, String password) {
        User user = userRepository.Login(username, password);
        switch (user.getRole()) {
            case "doctor":
                return "doctor";
            case "caregiver":
                return "caregiver";
            case "patient":
                return "patient";
        }
        return "ERROR";
    }
}
