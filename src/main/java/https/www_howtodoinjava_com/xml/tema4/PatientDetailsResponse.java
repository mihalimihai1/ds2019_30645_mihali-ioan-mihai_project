//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.7 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.01.17 at 06:28:35 AM UTC 
//


package https.www_howtodoinjava_com.xml.tema4;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PatientWS" type="{https://www.howtodoinjava.com/xml/tema4}PatientWS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "patientWS"
})
@XmlRootElement(name = "PatientDetailsResponse")
public class PatientDetailsResponse {

    @XmlElement(name = "PatientWS", required = true)
    protected PatientWS patientWS;

    /**
     * Gets the value of the patientWS property.
     * 
     * @return
     *     possible object is
     *     {@link PatientWS }
     *     
     */
    public PatientWS getPatientWS() {
        return patientWS;
    }

    /**
     * Sets the value of the patientWS property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientWS }
     *     
     */
    public void setPatientWS(PatientWS value) {
        this.patientWS = value;
    }

}
