import {HOST} from '../../../commons/hosts';
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_medications: '/medications/',
    post_medications: "/medications/",
    delete_medications: "/medications/",
    update_medications: "/medications"
};

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: 'GET',
    });
    RestApiClient.performRequest(request, callback);
}

function getMedicationsById(params, callback){
    let request = new Request(HOST.backend_api + endpoint.get_medications + params.id, {
        method: 'GET'
    });
    RestApiClient.performRequest(request, callback);
}

function updateMedications(medications, callback){
    let request = new Request(HOST.backend_api + endpoint.update_medications , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medications)
    });
    RestApiClient.performRequest(request, callback);
}

function addMedications(medications, callback){
    let request = new Request(HOST.backend_api + endpoint.post_medications , {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medications)
    });
    RestApiClient.performRequest(request, callback);
}

function deleteMedications(medications, callback){
    let request = new Request(HOST.backend_api + endpoint.delete_medications , {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medications)
    });
    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    getMedicationsById,
    updateMedications,
    addMedications,
    deleteMedications
};
