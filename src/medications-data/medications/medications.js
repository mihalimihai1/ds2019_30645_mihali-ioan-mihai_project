import React from 'react';
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import {Card, Col, Row} from 'reactstrap';
import Table from "../../commons/tables/table"
import * as API_USERS from "./api/medications-api"
import MedicationsForm from "./medications-form";

const columns = [
    {
        Header:  'medications_id',
        accessor: 'medications_id',
    },
    {
        Header: 'dosage',
        accessor: 'dosage',
    },
    {
        Header: 'list_effects',
        accessor: 'list_effects',
    },
    {
        Header: 'name',
        accessor: 'name',
    },
];

const filters = [];

class Medications extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {

            if(result !== null && status === 200) {
                result.forEach( x => {
                    this.tableData.push({
                        medications_id: x.medications_id,
                        dosage: x.dosage,
                        list_effects: x.list_effects,
                        name: x.name,
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh(){
        this.forceUpdate()
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.tableData}
                                columns={columns}
                                search={filters}
                                pageSize={pageSize}
                            />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationsForm
                                    registerMedications={this.refresh}
                                    updateMedications={this.refresh}>
                                </MedicationsForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 &&
                <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>}

            </div>
        );
    };

}

export default Medications;
